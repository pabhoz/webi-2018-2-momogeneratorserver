<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET,PUT,POST,DELETE");
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
	/**
        EasyDBManager TEST	
    */
		require_once './MySQLiManager.php';
        
        function seleccionar(){
            $db = new MySQLiManager('localhost','root','','momogenerator');
            $momos = $db->select("*","Momo");
            $captions = $db->select("*","Caption");

            foreach ($momos as $i => $momo) {
                $momos[$i]["captions"] = [];
                foreach ($captions as $j => $caption) {
                    if($caption["momoId"] == $momo["id"]){
                        $temp["x"] = $caption["x"];
                        $temp["y"] = $caption["y"];
                        $temp["value"] = $caption["value"];
                        $momos[$i]["captions"][] = $temp;
                        unset($captions[$j]);
                    }
                }
            }
            print(json_encode($momos,true));
        }

        seleccionar();